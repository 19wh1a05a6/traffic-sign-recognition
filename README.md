Traffic sign recognition is a project in which we build a
deep neural network model that can classify traffic signs present in the image into
different categories.
